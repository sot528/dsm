DSM
===

# 概要
- DSMは非中央集権化された勉強会(Decentralized Study Meeting)であり、DAOとして機能する

## ゴール
- 勉強会が自律分散的に運営され、滞りなく定期的に実施されること
- トラストレスなDAOとして構築・運用されること

## 完全にトラストレスなDAOとして実装する
- トラストが必要なコントラクトの更新は行わない
    - コントラクトに更新が必要な場合はデプロイをし直し別のDAOとして再スタートする
- ブロックチェーン以外のインフラについてもDecentralizedな技術を用いる
    - フロントエンドはIFPSへデプロイする
    - バックエンドは持たない

## 本リポジトリの位置づけ
- 本リポジトリは汎用的なDSMの実装であると同時にリファレンス実装である
- リファレンス実装としての固有ルールは以下の通り
    - 勉強会の開催場所は東京23区内とする
    - 開催に費用がかかる場合は主催者の負担とする
    - 会のテーマはDecentralizedである

# ルール
DAOは以下のルールをスマートコントラクトとして実装する。

## 参加者
- DSMに一定金額のデポジットを行うことで参加者となれる
    - デフォルトは1ETHである
- 参加者は勉強会に参加する権利を持つ
- 参加者は持ち回りで勉強会を主催する義務がある
- 参加者はいつでも退会しデポジットを引き出すことが可能
    - 例外: 主催期間中(自分が主催者である期間中)はデポジットロックされる
- 参加者数には上限が設けられている
    - デフォルトは8人である

## 開催周期
- 一定期間に一度
    - デフォルトは5週間(35日間)に1度である

## 主催者
- 主催者は開催周期ごとに全参加者の中から1人が選任される
    - 選任順序は不平等がおこらない形とする

## 参加者の投票と勉強会の成否
- 主催者以外の参加者は、開催周期ごとに勉強会の合否を問う投票を行える
- 今回の勉強会が成功と判断したら成功投票を行う
- 今回の勉強会が不成功と判断したら何も行わない
    - 成功投票を行わなければ、ファイナライズの際に自動的に不成功への投票として扱われる
    
## デポジットのBurn
- 

## ファイナライズ
- 開催周期が過ぎれば、参加者は誰でもファイナライズが実行可能である
- ファイナライズでは以下の処理が行われる
    - 今回の勉強会の成否判定
        - 主催者以外の参加者の半数以上が成功と判定した場合は成功である
        - 失敗だった場合は主催者のデポジットをBurnして除名
    - 次の主催者の選任

## 備考
- 最低2人が参加者として登録するまでDSMは機能しない
    - 主催者は選任されない
    - 参加者のデポジットが没収されることは無い